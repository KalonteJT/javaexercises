public abstract class Account implements Detailables{

    private double balance;
    private String name;
    private static double interestRate;

    

    public String getDetails(){
        return "" + getName() + "'s Balance:" + getBalance();
    }
    public double getBalance() {
        return this.balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public abstract void addInterest();

    //this.balance = (double)(this.balance * interestRate);
    public Account(double balance, String name) {
        this.balance = balance;
        this.name = name;
    }
    
    public Account(){
        this(1000,"Test Name");
    }

    public static double getInterestRate() {
        return interestRate;
    }

    public static void setInterestRate(double interestRate) {
        Account.interestRate = interestRate;
    }

    public boolean withdraw(double amount){
        if(this.balance>amount){
            this.balance = this.balance - amount;
            return true;
        }
        System.out.println("There's not enough money in this account");
        return false;
    }

    public boolean withdraw(){
        if(this.balance>20){
            this.balance = (double)(this.balance - 20);
            return true;
        }
        System.out.println("There's not enough money in this account");
        return false;
    }
    
}