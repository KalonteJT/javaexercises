import java.util.HashSet;
import java.util.Iterator;
import java.util.TreeSet;

public class CollectionsTest {
  public static void main(String[] args) {
    HashSet<Account> accounts;
    accounts = new HashSet<Account>();
    //accounts.add(new SavingsAccount(5000, "Hash"));
    //accounts.add(new SavingsAccount(6000, "Slinging"));
    //accounts.add(new CurrentAccount(7000, "Slasher"));

    Iterator<Account> iterateValue = accounts.iterator();
    System.out.println("Iterator");
    while(iterateValue.hasNext()){
      Account currentAccount = iterateValue.next();
      System.out.println(currentAccount.getDetails());
      currentAccount.addInterest();
      System.out.println(currentAccount.getDetails());
    }
    System.out.println("ForEach");
    for (Account account : accounts) {
      System.out.println(account.getDetails());
      account.addInterest();
      System.out.println(account.getDetails());
    }
    // Why does this print the elements added to the Hash list above
    TreeSet<Account> accountTree = new TreeSet<Account>(new AccountsComparator());
    accountTree.add(new SavingsAccount(2000, "Arielle"));
    accountTree.add(new SavingsAccount(480, "Zelda"));
    accountTree.add(new CurrentAccount(500, "Link"));
    System.out.println("TreeSet");
    for (Account account2 : accountTree) {
      System.out.println(account2.getDetails());
      account2.addInterest();
      System.out.println(account2.getDetails());
    }
    System.out.println("Done");


        
      }
    
}