public class TestAccount {

    public static void main(String[] args) {
        Account acc1 = new Account();
        acc1.setBalance(100);
        acc1.setName("Test");
        String testname = acc1.getName();
        double testbalance = acc1.getBalance();
        System.out.println("Balance "+testbalance);
        System.out.println("Name "+testname);
        acc1.addInterest();
        double newbalance = acc1.getBalance();
        System.out.println("Old Balance "+testbalance);
        System.out.println("New Balance "+newbalance);
        Account acc2 = new Account();
        double test2b = acc2.getBalance();
        System.out.println(test2b);
        System.out.println(newbalance);
        Account[] arrayOfAccounts = new Account[5];
        double[] balance_array = {322,123,675,321,765};
        String[] name_array = {"Kalonte","Tim","Fabuola","Jordan","Maher"};
        for(int i = 0; i < 5; i++){
            arrayOfAccounts[i] = new Account();
            arrayOfAccounts[i].setBalance(balance_array[i]);
            arrayOfAccounts[i].setName(name_array[i]);
            System.out.println(arrayOfAccounts[i].getName());
            System.out.println(arrayOfAccounts[i].getBalance());
            arrayOfAccounts[i].addInterest();
            System.out.println(arrayOfAccounts[i].getName()+"'s new balance is "+ arrayOfAccounts[i].getBalance());
        }

    }
    
}