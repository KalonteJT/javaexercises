package com.conygre.spring.example.controllers;

import java.util.Collection;

import com.conygre.spring.example.entities.Furry;
import com.conygre.spring.example.service.FurryService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;


@RestController
@RequestMapping("/furries")
public class FurryController {
    
    @Autowired
    private FurryService service;
    @RequestMapping(method=RequestMethod.GET)
    public Collection<Furry> getFurries() {
        return service.getFurries();
    }

    @RequestMapping(method=RequestMethod.POST)
    public void addFurry(@RequestBody Furry furry){
        service.addFurry(furry);
    }
}