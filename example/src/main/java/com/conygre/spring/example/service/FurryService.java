package com.conygre.spring.example.service;

import java.util.Collection;

import com.conygre.spring.example.entities.Furry;

public interface FurryService {
    
    Collection<Furry> getFurries();
    void addFurry(Furry furry);
}