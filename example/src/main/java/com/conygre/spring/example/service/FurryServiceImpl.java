package com.conygre.spring.example.service;

import java.util.Collection;

import com.conygre.spring.example.entities.Furry;
import com.conygre.spring.example.repo.FurryRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FurryServiceImpl implements FurryService {

    @Autowired
    private FurryRepository repo;

    @Override
    public Collection<Furry> getFurries() {
        return repo.findAll();
    }

    @Override
    public void addFurry(Furry furry) {
        // TODO Auto-generated method stub
        repo.insert(furry);
    }
    
}